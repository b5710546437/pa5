package CheapClock;

/**
 * Run the CheapClock.
 * @author Arut Thanomwatana
 *
 */
public class Main 
{
	public static void main(String[]args){
		Clock clock = new Clock();
		CheapClockUI cheap = new CheapClockUI(clock);
		cheap.run();
		
	}

}
